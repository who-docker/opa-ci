FROM alpine:3.16

COPY --from=openpolicyagent/opa /opa /opa

USER 0

ENTRYPOINT ["/usr/bin/env"]
CMD ["/bin/sh"]
